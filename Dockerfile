FROM tomcat:8
LABEL Author="Paolo Mazzoni <ing.paolo.mazzoni@gmail.com>"

RUN mkdir -p /home/ampli-sync/working\ dir/config/
ADD ["files/SqliteSync-3.2.16.war", "/usr/local/tomcat/webapps/"]
ADD ["files/sync.properties", "/home/ampli-sync/working dir/config/"]
RUN mv /usr/local/tomcat/webapps/SqliteSync-3.2.16.war /usr/local/tomcat/webapps/sqlitesync.war

# http://localhost/sqlitesync/API3